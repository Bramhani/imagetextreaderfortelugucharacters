# -*- coding: utf-8 -*-
from skimage.measure import compare_ssim as ssim
import matplotlib.pyplot as plt
import numpy as np
import cv2
import glob														
import sys

print("----------------------------------------------WELCOME TO THE IMAGE CHARACTER RECOGNITION IN TELUGU-------------------------------------------------")

def mse(imageA, imageB):
	err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
	err /= float(imageA.shape[0] * imageA.shape[1])
	return err


def cal_ssim(imageA, imageB, title):
	m = mse(imageA, imageB)
	s = ssim(imageA, imageB)

	fig = plt.figure(title)
	plt.suptitle("MSE: %.2f, SSIM: %.2f" % (m, s))
	

	ax = fig.add_subplot(1, 2, 1)
	plt.imshow(imageA, cmap = plt.cm.gray)
	plt.axis("off")

	ax = fig.add_subplot(1, 2, 2)
	plt.imshow(imageB, cmap = plt.cm.gray)
	plt.axis("off")

	print('SSIM: %f' %s, 'MSE: %f' %m)
	#plt.show()
	return s

X_data = []
files = glob.glob ("*.png")
for myFile in files:
    #print(myFile)
    image = cv2.imread (myFile)
    X_data.append (image)

gray = []
for i in X_data:
	image = cv2.cvtColor(i, cv2.COLOR_BGR2GRAY)
	gray.append(image)
	
given = cv2.imread(sys.argv[1])
h, w = given.shape[:2]
print(h)
print(w)
#if(h==320 and w==370):
given = cv2.cvtColor(given, cv2.COLOR_BGR2GRAY)
#else:
	#given = cv2.resize(given,(320,370))
	#given = cv2.cvtColor(i, cv2.COLOR_BGR2GRAY)
	#h, w = given.shape[:2]
	#print(h)
	#print(w)
fig = plt.figure("given Image")
plt.imshow(given, cmap = plt.cm.gray)
plt.axis("off")
plt.show()
	
m = []
ssi = []
for i in gray:
	r = mse(i, given)
	p = cal_ssim(i, given, "REFERENCE IMAGE2 r2 vs. GIVEN")
	m.append(r);
	ssi.append(p);
	
max = 0;
k=0;
for i in  range(len(ssi)):
	if(max<ssi[i]):
		max = ssi[i]
		k = i+1;
	
print(max)
f = open("result.txt", "w")

if(k==1):
	f.write('ట')
elif k==2:
	f.write('భ')
elif k==3:
	f.write('ఓ')
elif k==4:
	f.write('ఉ')
elif k==5:
	f.write('ష')
elif k==6:
	f.write('చ')
elif k==7:
	f.write('క')
elif k==8:
	f.write('ల')
elif k==9:
	f.write('వ')
elif k==10:
	f.write('ళ')
elif k==11:
	f.write('ఔ')
elif k==12:
	f.write('మ')
elif k==13:
	f.write('థ')
elif k==14:
	f.write('ఫ')
elif k==15:
	f.write('ప')
elif k==16:
	f.write('ధ')
elif k==17:
	f.write('ఈ')
elif k==18:
	f.write('క్ష')
elif k==19:
	f.write('య')
elif k==20:
	f.write('ఱ')
elif k==21:
	f.write('ఐ')
elif k==22:
	f.write('అః')
elif k==23:
	f.write('ఆ')
elif k==24:
	f.write('ఛ')
elif k==25:
	f.write('త')
elif k==26:
	f.write('డ')
elif k==27:
	f.write('ఇ')
elif k==28:
	f.write('బ')
elif k==29:
	f.write('ఒ')
elif k==30:
	f.write('ఞ')
elif k==31:
	f.write('ఋా')
elif k==32:
	f.write('గ')
elif k==33:
	f.write('ఝ')
elif k==34:
	f.write('ఎ')
elif k==35:
	f.write('ద')
elif k==36:
	f.write('ఘ')
elif k==37:
	f.write('ఏ')
elif k==38:
	f.write('న')
elif k==39:
	f.write('జ')
elif k==40:
	f.write('అ')
elif k==41:
	f.write('ణ')
elif k==42:
	f.write('ఠ')
elif k==43:
	f.write('ఖ')
elif k==44:
	f.write('ఋ')
elif k==45:
	f.write('ఙ')
elif k==46:
	f.write('ఊ')
elif k==47:
	f.write('హ')
elif k==48:
	f.write('ఢ')
elif k==49:
	f.write('అం')
elif k==50:
	f.write('ర')
elif k==51:
	f.write('శ')
elif k==52:
	f.write('స')

f.close()
	
print("\v\t<<<<<<<<<<<<<<<<<<<<CHECK FOR TELUGU LETTER FILE FOR THE RESULT>>>>>>>>>>>>>>>")

